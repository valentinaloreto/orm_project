require_relative 'db/connection'
require_relative 'lib/orm_challenge/agent'
require_relative 'lib/orm_challenge/bl_house'
require_relative 'lib/orm_challenge/bl_master'
require_relative 'lib/orm_challenge/client'
require_relative 'lib/orm_challenge/company'
require_relative 'lib/orm_challenge/container'
require_relative 'lib/orm_challenge/kind'
require_relative 'lib/orm_challenge/ship'
require_relative 'lib/orm_challenge/size'
require_relative 'lib/orm_challenge/voyage'
require_relative 'lib/orm_challenge/country'
require 'date'

module Main
  attr_accessor :choice
  attr_reader :menu

  def self.build_menu
    @menu = "\n\nPlease choose which query you wish to execute: \n\n"
    @menu += "   1. Bill of Landing (Master) info\n\n"
    @menu += "   2. Bill of Landing (House) info\n\n"
    @menu += "   3. List of containers by their type\n\n"
    @menu += "   4. All BL Master's by company\n\n"
    @menu += "   5. Top 10 destination countries by date\n\n"
  end

  def self.get_user_choice
    loop do
      print "Your choice: "
      @choice = gets.chomp.to_i
      break if (1..5).to_a.include?( @choice )
      print "\n\nInvalid choice. Please, try again\n\n"
    end
  end

  def self.sort_choice
    system('clear')
    case @choice
    when 1
      Blmaster.single_report
    when 2
      Blhouse.single_report
    when 3
      Container.count_report
    when 4
      Blmaster.by_company_report
    when 5
      Voyage.destinations_report
    end
  end

  def self.beginning
    system('clear')
    build_menu
    print @menu
    get_user_choice
    sort_choice
  end
end

Main.beginning





