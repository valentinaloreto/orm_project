class Size < ActiveRecord::Base
  has_many :containers

  def self.up
    self.create({name: "40 feet"})
    self.create({name: "20 feet"})
  end

  def self.down
    self.delete_all
  end

end
