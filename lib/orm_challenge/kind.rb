class Kind < ActiveRecord::Base
  has_many :containers

  def self.up
    self.create({name: "ST"})
    self.create({name: "HQ"})
  end

  def self.down
    self.delete_all
  end

end
