class Agent < ActiveRecord::Base
  has_many :blmasters

  def self.up( num )
    num.times do |i|
      self.create(name: "Agent ##{i}")
    end
  end

  def self.down
    self.delete_all
  end

end
