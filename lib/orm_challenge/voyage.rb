
class Voyage < ActiveRecord::Base
  has_one :blmaster
  #has_many :countries
  belongs_to :destination,  class_name: "Country"
  belongs_to :origin,  class_name: "Country"


  attr_accessor :date, :voyages

  def self.up( num )
    countries_ids = Country.all.map(&:id)
    num.times do |i|
      d = DateTime.now - ( rand * 365 )
      self.create({
        code: "#{ ('A'..'Z').to_a.sample(2).join }-#{ rand(1000..9000) }",
        origin_id: countries_ids.sample,
        destination_id: countries_ids.sample,
        departure_date: d,
        arrival_date: d + rand(7..60)
      })
    end
  end

  def self.down
    self.delete_all
  end
#############################################################################
  def self.get_user_input
    begin
      print "\nPlease, give me a valid date (e.g. 2017-03-20): "
      @date = DateTime.parse( gets.chomp.to_s )
    rescue
      retry
    end
  end

  def self.get_all
      @voyages = Voyage.group(:destination_id).where("voyages.arrival_date < ?", @date ).limit(10).count.sort_by {|k,v| v}.reverse
  end

  def self.render
    @voyages.each do |voyage|
      print "\n   Country: #{  Country.find( voyage[0] ).name }"
      print "\n   Number of ships arrived: #{ voyage[1] }\n\n"
    end
  end

  def self.destinations_report
    get_user_input
    get_all
    render
  end
end
