class Client < ActiveRecord::Base
  has_one :blhouse
  has_and_belongs_to_many :containers

  def self.up( num )
    num.times do |i|
      self.create(name: "Client ##{i}")
    end
  end

  def self.down
    self.delete_all
  end

end
