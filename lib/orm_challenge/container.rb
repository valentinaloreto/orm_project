class Container < ActiveRecord::Base

  belongs_to :size
  belongs_to :kind
  belongs_to :company
  has_and_belongs_to_many :clients

  attr_accessor :result

  def self.up( num )
    sizes_ids = Size.all.map(&:id)
    kinds_ids = Kind.all.map(&:id)
    companies_ids = Company.all.map(&:id)

    num.times do |i|
      self.create({
        size_id: sizes_ids.sample,
        kind_id: kinds_ids.sample,
        company_id: companies_ids.sample,
        code: "#{ ('A'..'Z').to_a.sample(4).join }-#{ rand(1000000..9000000) }"
      })
    end
  end

  def self.down
    self.delete_all
  end

  #####################################################################################

  def self.get_all
    @result = []
    containers_count = Container.group(:kind_id ).count
    containers_count.each do |k,v|
      kind = Kind.find( k )
      r = "\n  Type of Container: #{ kind.name }\n"
      r += "  Quantity:  #{ v }\n\n"
      @result << r
    end
  end

  def self.print_results
    @result.each { |i| print i }
  end

  def self.count_report
    get_all
    print_results
  end
end
