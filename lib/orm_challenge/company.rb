
class Company < ActiveRecord::Base
  has_many :blmasters
  has_many :ships
  has_many :containers
  attr_accessor :names

  NAMES = [
    "Advanced Micro Devices, Inc.", "AdvancePCS, Inc.", "Advantica Restaurant Group, Inc.", "The AES Corporation", "Aetna Inc.", "AFLAC Incorporated", "AGCO Corporation", "Brightpoint, Inc.", "Brinker International, Inc.", "Bristol-Myers", "Squibb Company", "Broadwing, Inc.", "Brown Shoe Company, Inc.", "Brown-Forman Corporation", "Brunswick Corporation", "Evergreen Marine Corporation", "CMA-CGM", "Maersk", "MSC", "Hapang-Lloyd", "APL, Inc.", "Cosco, Inc.", "Hanjin", "CSCL"
  ]

  def self.up( num )
    num.times do |i|
      self.create({
        name: "#{ NAMES.sample }-#{i}"
      })
    end
  end

  def self.down
    self.delete_all
  end

end
