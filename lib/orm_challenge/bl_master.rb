class Blmaster < ActiveRecord::Base
  belongs_to :agent
  belongs_to :company
  belongs_to :ship
  belongs_to :voyage
  has_many :blhouses

  attr_accessor :bl, :arr_all, :blmaster

  def self.up( num )
    companies_ids = Company.all.map(&:id)
    agents_ids = Agent.all.map(&:id)
    voyages_ids = Voyage.all.map(&:id)

    num.times do |i|
      company_id = companies_ids.sample
      ships = Ship.where("company_id = '#{ company_id }'")
      self.create({
        ship_id: ships.sample.id,
        company_id: company_id,
        agent_id: agents_ids.sample,
        voyage_id: voyages_ids.sample,
        code: "##{ ('A'..'Z').to_a.sample(5).join }-#{ rand(100..900) }"
      })
    end
  end

  def self.down
    self.delete_all
  end
  #####################################################################################
  def self.get_user_input
    loop do
      print "\nPlease, give me a valid company id: "
      get_all( gets.chomp.to_i )
      break unless @arr_all.empty?
      print "\nThat was an invalid choice. Please, try again\n"
    end
  end

  def self.get_all( company_id )
    @arr_all = self.where("company_id = '#{ company_id }'")
  end

  def self.render
    @arr_all.each do |item|
      msg = "\n  BL Master id: #{ item.id }\n"
      msg += "  BL Master Code: #{ item.code }\n"
      msg += "  Ship: #{ Ship.find( item.ship_id ).name }\n"
      msg += "  Company: #{ Company.find( item.company_id ).name }\n"
      msg += "  Agent: #{ Agent.find( item.agent_id ).name }\n"
      msg += "  Voyage id: #{ item.voyage_id }\n\n"
      print msg
    end
    print "  There are #{ @arr_all.size } BL masters in total\n\n"
  end

  def self.by_company_report
    get_user_input
    render
  end
  ############################################################################
  def self.get_bl_id
    begin
      print "\nPlease, give me a valid BL Master id: "
      @blmaster = self.find( gets.chomp.to_s )
    rescue
      retry
    end
  end

  def self.build_bl( blmaster )
    @bl = {}
    voyage = blmaster.voyage
    @bl['code'] = blmaster.code
    @bl['company'] = Company.find( blmaster.company_id ).name
    @bl['ship'] = Ship.find( blmaster.ship_id ).name
    @bl['agent'] =  Agent.find( blmaster.agent_id ).name
    @bl['origin'] = Country.find( voyage.origin_id ).name
    @bl['destination'] = Country.find( voyage.destination_id ).name
    @bl['arrival_date'] = voyage.arrival_date
    @bl['voyage_code'] = voyage.code
    arr = []
    total = 0
    blmaster.blhouses.each do |blhouse|
      blhouse.client.containers.each { |c| arr << c.code }
      total += blhouse.client.containers.size
    end
    @bl['containers_count'] = total
    @bl['containers_list'] = arr
    @bl
  end

  def self.render_bl
    @bl.each do |k,v|
      if k == 'containers_list'
        print "    #{ k }:   "
        v.each { |i| print "#{ i } | " }
        print "\n\n"
      else
        print "    #{k}: #{v}\n"
      end
    end
  end

  def self.single_report
    get_bl_id
    build_bl( @blmaster )
    render_bl
  end
end
