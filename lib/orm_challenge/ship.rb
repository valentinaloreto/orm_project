class Ship < ActiveRecord::Base
  has_many :blmasters
  attr_accessor :names

  NAMES = [
    "Abbey", "Ace", "Aesop", "Afrika", "Aggie", "Ajax", "Alpha", "Alfie", "Ali", "Aladdin",
    "Babe", "Baby", "Ballerina", "Bam", "Bam", "Bambi", "Bandera", "Bandit","Drake", "Dream", "Dreamer", "Drifter", "Duce", "Duchess", "Duke", "Fletch", "Flicka", "Freckles", "Frida", "Friday", "Frisky", "Hashtag", "Hawk", "Hercules", "Jesso", "Jester", "Jet", "Poncho", "Poppy", "Prairie", "Prancer", "Prankster", "Presley", "Pride"
  ]

  def self.up( num )
    companies_ids =  Company.all.map(&:id)
    num.times do |i|
      self.create({
        name: NAMES.sample,
        company_id:companies_ids.sample,
      })
    end
  end

  def self.down
    self.delete_all
  end
end
