class Country < ActiveRecord::Base
  has_many :voyages

  COUNTRIES = [
  'England','Japan','South Africa','Chile','New Zealand','Canada','Panama',
  'Spain', 'Uruguay', 'Brazil','Iceland','Madagascar', 'Singapour', 'China',
  'USA', 'Ireland', 'Morocco', 'France', 'Portugal', 'Finland', 'Thailand',
  'Australia', 'Indonesia', 'India'
  ]

  def self.up
    COUNTRIES.each do |country|
      self.create(name: "#{ country }")
    end
  end

  def self.down
    self.delete_all
  end
end
