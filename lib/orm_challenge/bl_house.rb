class Blhouse < ActiveRecord::Base
  belongs_to :blmaster
  belongs_to :client

  attr_accessor :blhouse, :bl

  def self.up( num )
    blmasters_ids = Blmaster.all.map(&:id)
    clients_ids = Client.all.map(&:id)

    num.times do |i|
      self.create({
        blmaster_id: blmasters_ids.sample,
        client_id: clients_ids.sample,
      })
    end
  end

  def self.down
    self.delete_all
  end
  ############################################################################
  def self.get_bl_id
    begin
      print "\nPlease, give me a valid BL House id: "
      @blhouse = self.find( gets.chomp.to_s )
    rescue
      retry
    end
  end

  def self.build_bl
    @blmaster = Blmaster.find ( @blhouse.blmaster_id )
    @bl = Blmaster.build_bl( @blmaster )
    @bl['containers_count'] = Client.find( @blhouse.client_id ).containers.size

    arr = []
    @blhouse.client.containers.each do |container|
       arr << container
    end
    @bl['containers_list'] = arr

    @bl['bl_master_id'] = @blhouse.blmaster_id
    @bl['client'] =  Client.find( @blhouse.client_id ).name
  end

  def self.render_bl
    @bl.each do |k,v|
      if k == 'containers_list'
        print "    #{ k }: "
        v.each { |i| print "#{ i.code } " }
        print "\n"
      else
        print "    #{k}: #{v}\n" unless k == 'code' || k == 'agent'
      end
    end
  end

  def self.single_report
    get_bl_id
    build_bl
    render_bl
  end
end
