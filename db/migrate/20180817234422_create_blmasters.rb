class CreateBlmasters < ActiveRecord::Migration[5.2]
  def change
    create_table :blmasters do |t|
      t.string :code, null: false
      t.references :ship, foreign_key: true, null: false
      t.references :company, foreign_key: true, null: false
      t.references :agent, foreign_key: true, null: false
      t.references :voyage, foreign_key: true, null: false
    end
  end
end
