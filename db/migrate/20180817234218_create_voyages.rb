class CreateVoyages < ActiveRecord::Migration[5.2]
  def change
    create_table :voyages do |t|
      t.string :code, null: false
      t.references :origin, index:true, foreign_key: {to_table: :countries}, null: false
      t.references :destination, index:true, foreign_key: {to_table: :countries}, null: false
      t.datetime :departure_date, null: false
      t.datetime :arrival_date, null: false
    end
  end
end
