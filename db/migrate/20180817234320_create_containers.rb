class CreateContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :containers do |t|
      t.references :size, foreign_key: true, null: false
      t.references :kind, foreign_key: true, null: false
      t.references :company, foreign_key: true, null: false
      t.string :code, null: false
    end
  end
end
