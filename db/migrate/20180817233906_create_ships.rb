class CreateShips < ActiveRecord::Migration[5.2]
  def change
    create_table :ships do |t|
      t.string :name, null: false
      t.references :company, foreign_key: true, null: false
    end
  end
end
