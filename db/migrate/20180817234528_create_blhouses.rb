class CreateBlhouses < ActiveRecord::Migration[5.2]
  def change
    create_table :blhouses do |t|
      t.references :blmaster, foreign_key: true, null: false
      t.references :client, foreign_key: true, null: false
    end
  end
end
