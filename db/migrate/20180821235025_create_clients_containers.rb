class CreateClientsContainers < ActiveRecord::Migration[5.2]
  def change
    create_table :clients_containers do |t|
      t.references :client, foreign_key: true
      t.references :container, foreign_key: true
    end
  end
end

