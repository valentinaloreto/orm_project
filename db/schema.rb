# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_21_235126) do

  create_table "agents", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "blhouses", force: :cascade do |t|
    t.integer "blmaster_id", null: false
    t.integer "client_id", null: false
    t.index ["blmaster_id"], name: "index_blhouses_on_blmaster_id"
    t.index ["client_id"], name: "index_blhouses_on_client_id"
  end

  create_table "blmasters", force: :cascade do |t|
    t.string "code", null: false
    t.integer "ship_id", null: false
    t.integer "company_id", null: false
    t.integer "agent_id", null: false
    t.integer "voyage_id", null: false
    t.index ["agent_id"], name: "index_blmasters_on_agent_id"
    t.index ["company_id"], name: "index_blmasters_on_company_id"
    t.index ["ship_id"], name: "index_blmasters_on_ship_id"
    t.index ["voyage_id"], name: "index_blmasters_on_voyage_id"
  end

  create_table "clients", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "clients_containers", force: :cascade do |t|
    t.integer "client_id"
    t.integer "container_id"
    t.index ["client_id"], name: "index_clients_containers_on_client_id"
    t.index ["container_id"], name: "index_clients_containers_on_container_id"
  end

  create_table "companies", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "containers", force: :cascade do |t|
    t.integer "size_id", null: false
    t.integer "kind_id", null: false
    t.integer "company_id", null: false
    t.string "code", null: false
    t.index ["company_id"], name: "index_containers_on_company_id"
    t.index ["kind_id"], name: "index_containers_on_kind_id"
    t.index ["size_id"], name: "index_containers_on_size_id"
  end

  create_table "countries", force: :cascade do |t|
    t.string "name"
  end

  create_table "kinds", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "ships", force: :cascade do |t|
    t.string "name", null: false
    t.integer "company_id", null: false
    t.index ["company_id"], name: "index_ships_on_company_id"
  end

  create_table "sizes", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "voyages", force: :cascade do |t|
    t.string "code", null: false
    t.integer "origin_id", null: false
    t.integer "destination_id", null: false
    t.datetime "departure_date", null: false
    t.datetime "arrival_date", null: false
    t.index ["destination_id"], name: "index_voyages_on_destination_id"
    t.index ["origin_id"], name: "index_voyages_on_origin_id"
  end

end
